# webpack-build

Boilerplate for webpack

# Usage

Start dev server
```
yarn start
```

Create production build to `dist/`
```
yarn build
```
